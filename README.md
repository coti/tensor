# please see the piranha branch 

## TODO

* Document the functions

## Later evolutions

* On-the-fly optimization

## Usage

Dependency: on Debian `libginac-dev ginac-tools`

Compilation:
```
mpic++ -O3 -g -Wall -o tensormatrix_mpi tensormatrix_mpi.cpp -lginac  -Wno-unused-variable
```

Running:
```
mpiexec -n 17 ./tensormatrix_mpi
```

Get some help:
```
mpiexec -n 1 ./tensormatrix_mpi h
```

The previous line will display:
```
Arguments:
(mpiexec .... ) ./tensormatrix_mpi [N] [Function name] [Nb of foremen] : run the program
(mpiexec .... ) ./tensormatrix_mpi h                                   : display help
Function names being: 
 - M/m: Master-Worker -> multiply_1level_mw
 - A/a: Master-Worker, addition on a slave -> multiply_1level_mw_addslave
 - H/h: Hierarchical master-worker -> multiply_1level_mw_hierarch
 - S/s: Sequential -> multiply_seq
 - 1  : Sequential, 1 level of decomposition -> multiply_1level
 - 2  : Sequential, 2 levels of decomposition -> multiply_2levels
```

For instance, to run the hierarchical master-worker on 17 processes with 4 foremen, on a tensor of size 8:

```
mpiexec -n 17 ./tensormatrix_mpi 8 h 4
```
