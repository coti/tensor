#ifndef _PARALL_INTERNAL_H_
#define _PARALL_INTERNAL_H_

gi::ex multiply_1level_master( tensor3D_t&, matrix_int_t&, unsigned int, MPI_Comm comm );

#endif // ndef _PARALL_INTERNAL_H_
