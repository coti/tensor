#ifndef _UTILS_PARALL_H_
#define _UTILS_PARALL_H_

#include <ginac/ginac.h>
namespace gi = GiNaC;

/*******************************************************************************
 *                              Datatypes                                      *
 *******************************************************************************/

class parameters_t{
public:
    unsigned int a4, a2, a1;
    parameters_t( unsigned int, unsigned int, unsigned int );
    parameters_t( void ){};
};

class parameters_s_t{
public:
    unsigned int a4;
    parameters_s_t( unsigned int );
    parameters_s_t( void ){};
};

class parameters_2_1_t{
public:
    unsigned int a4, a2;
    parameters_2_1_t( unsigned int, unsigned int );
    parameters_2_1_t( void ){};
    void setA4( unsigned int _a4 ) { this->a4 = _a4; }
    void setA2( unsigned int _a2 ) { this->a2 = _a2; }
    void setParams( unsigned int _a4, unsigned int _a2 ) { this->a4 = _a4; this->a2 = _a2; } ;
};

class parameters_2_2_t{
public:
    unsigned int a4, a2, a6, a1;
    parameters_2_2_t( unsigned int, unsigned int, unsigned int, unsigned int );
    parameters_2_2_t( void ){};
};


/*******************************************************************************
 *                             Prototypes                                      *
 *******************************************************************************/

std::string linearize_expression( gi::ex );
gi::ex de_linearize_expression( std::string, gi::lst );

void send_work( std::vector<parameters_t>& input, int peer, MPI_Comm comm = MPI_COMM_WORLD );
void send_work( std::vector<parameters_2_2_t>& input, int peer, MPI_Comm comm = MPI_COMM_WORLD );
void send_work( std::vector<parameters_2_1_t>& input, int peer, MPI_Comm comm = MPI_COMM_WORLD );
void send_work( std::vector<parameters_s_t>& input, int peer, MPI_Comm comm = MPI_COMM_WORLD );

void send_expressions_to_add( std::vector<std::string>&, int );
void send_expressions_to_add( std::vector<std::string>&, int, parameters_2_1_t );
void send_expressions_to_add( std::vector<std::string>&, int, parameters_s_t );
void send_add_or_end_addslave(  std::vector<std::string>&, int, int* );
void send_add_or_end_addslave(  std::vector<std::string>&, int, int*, parameters_s_t );
void send_add_or_end_addslave(  std::vector<std::string>&, int, int*, parameters_2_1_t );
void send_work_addslave(  std::vector<parameters_t>&, std::vector<std::string>&, int ) ;
void send_work_addslave(  std::vector<parameters_s_t>&, std::vector<std::string>&, int ) ;
void send_work_addslave(  std::vector<parameters_2_1_t>&, std::vector<std::string>&, int );
void send_result( gi::ex T, MPI_Comm comm = MPI_COMM_WORLD );
void send_result( gi::ex T, int dst, MPI_Comm comm = MPI_COMM_WORLD );
void send_end( int peer, MPI_Comm comm = MPI_COMM_WORLD );
void send_end( int peer, parameters_2_1_t p, MPI_Comm comm = MPI_COMM_WORLD );
void send_end( int peer, parameters_2_2_t p, MPI_Comm comm = MPI_COMM_WORLD );
void send_end( int peer, parameters_s_t p, MPI_Comm comm = MPI_COMM_WORLD );
void send_end_batch( int peer, MPI_Comm comm = MPI_COMM_WORLD );
void send_end_batch( int peer, parameters_2_1_t p, MPI_Comm comm = MPI_COMM_WORLD );

gi::ex recv_result( int peer, gi::lst symbols,  MPI_Comm comm = MPI_COMM_WORLD );

void create_parameters_datatype( void );
void create_parameters_datatype_s( void );
void create_parameters_datatype_2_1( void );
void create_parameters_datatype_2_2( void );
void free_parameters_dt( void );
void free_parameters_2_1_dt( void );
void free_parameters_2_2_dt( void );
void free_parameters_s_dt( void );

gi::ex add_expressions( std::vector<std::string>, gi::lst );

void create_communicators_hierarch( MPI_Comm&, MPI_Comm& );

/*******************************************************************************
 *                            Global variables                                 *
 *******************************************************************************/

extern MPI_Datatype DT_PARAMETERS;
extern MPI_Datatype DT_PARAMETERS_2_1;
extern MPI_Datatype DT_PARAMETERS_2_2;
extern MPI_Datatype DT_PARAMETERS_S;

extern unsigned int nbforemen;     /* Number of foremen to use with the hierarchical M/W */
extern unsigned int maxresult;     /* Maximum results in the result queue, addslave version */

#endif // _UTILS_PARALL_H_
