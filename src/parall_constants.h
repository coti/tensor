#ifndef _PARALL_CONSTANTS_H_
#define _PARALL_CONSTANTS_H_

#define TAG_PULL  0   /* W -> M initial */
#define TAG_WORK  1   /* M -> W here is some work */
#define TAG_RES   2   /* W -> M I have a result */
#define TAG_EXPR  3   /* W -> M here is my result */
#define TAG_ADD   4   /* M -> W add these expressions */
// NOT NECESSARY #define TAG_ADDED 5   /* W -> M here is the result of the addition */

#define TAG_END_BATCH  6  /* F -> W: this batch is done but more work might come */
#define TAG_END   7   /* M -> W we are done */

#endif // ndef _PARALL_CONSTANTS_H_
