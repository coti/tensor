#ifndef _PROFILING_H_
#define _PROFILING_H_

#ifdef TAUPROF
#include <TAU.h>
#else
#define TAU_START( a ) do { ;; }while( 0 );
#define TAU_STOP( a ) do { ;; } while( 0 );
#endif // TAUPROF

double getTimeSpent( void* );
double getTimeSpent( const char* );
uint64_t rdtsc( void );

#endif // _PROFILING_H_
