#include <iostream>
#include "profiling.h"

#ifdef TAUPROF
double previous = 0;

double getTimeSpent( void* ptr ){
     
  long calls, childcalls;
  double incl[TAU_MAX_COUNTERS], excl[TAU_MAX_COUNTERS];
  const char **counters;
  int numcounters, j; 

  TAU_PROFILER_GET_CALLS(ptr, &calls);
  TAU_PROFILER_GET_CHILD_CALLS(ptr, &childcalls);
  TAU_PROFILER_GET_INCLUSIVE_VALUES(ptr, &incl);
  TAU_PROFILER_GET_EXCLUSIVE_VALUES(ptr, &excl);

  TAU_PROFILER_GET_COUNTER_INFO(&counters, &numcounters);
  printf("Calls = %ld, child = %ld\n", calls, childcalls);
  printf("numcounters = %d\n", numcounters);
  for (j = 0; j < numcounters ; j++)  {
    printf(">>>");
    printf("counter [%d] = %s\n", j, counters[j]);
    printf(" excl [%d] = %g, incl [%d] = %g\n", j, excl[j], j, incl[j]);
  }
  return incl[0];
}


double getTimeSpent( const char* cnt ){
    
    const char **inFuncs;
    /* The first dimension is functions, and the 
       second dimension is counters */
    double **counterExclusiveValues;
    double **counterInclusiveValues;
    int *numOfCalls;
    int *numOfSubRoutines;
    const char **counterNames;
    int numOfCouns;
    int numOfFunctions;
    const char ** functionList;
    int i;

    numOfFunctions = 1;
    inFuncs = (const char **) malloc(sizeof(const char *) * numOfFunctions );
    inFuncs[0] = cnt;
    
    TAU_GET_FUNC_VALS( inFuncs, numOfFunctions,
                       counterExclusiveValues,
                       counterInclusiveValues,
                       numOfCalls,
                       numOfSubRoutines,
                       counterNames,
                       numOfCouns );
    
    i = 0;
    if( 0 == strcmp( cnt, inFuncs[i] ) ) {
      free( inFuncs );
      /*      double dur = numOfCalls[i] - previous;
	      previous = numOfCalls[i];*/
      double dur = counterExclusiveValues[i][0] - previous;
      previous = counterExclusiveValues[i][0];
      return dur;
    }
    
    std::cerr << "Timer " << cnt << " not found" << std::endl;
    free( inFuncs );
    return -1;
}
#else
double getTimeSpent( const char* cnt ){
    return 0;
}
#endif // def TAUPROF

uint64_t rdtsc(){
    unsigned int lo,hi;
    __asm__ __volatile__ ("rdtsc" : "=a" (lo), "=d" (hi));
    return ((uint64_t)hi << 32) | lo;
}
 
