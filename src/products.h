#ifndef _PRODUCTS_H_
#define _PRODUCTS_H_

#include <ginac/ginac.h>
#include "tensormatrix.h"

namespace gi = GiNaC;

// internal (sequential) routines
gi::ex one_level1_product( tensor3D_t*, int, int, int, int );
gi::ex one_level1_product( tensor3D_t*, int, int, int );
gi::ex one_level1_product( tensor3D_t*, int, int );
gi::ex one_level2_product( tensor3D_t*, int, int, int, int, int );
gi::ex two_level1_product( tensor3D_t*, int, int, int );
gi::ex two_level2_product( tensor3D_t*, int, int, int, int, int );
gi::ex two_level2_product( tensor3D_t*, int, int, int );

#endif // _PRODUCTS_H_
