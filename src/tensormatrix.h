#ifndef _TENSORMATRIX_H_
#define _TENSORMATRIX_H_

namespace gi = GiNaC;

/*******************************************************************************
 *                                Datatypes                                    *
 *******************************************************************************/

typedef std::vector<std::vector<std::vector<gi::ex> > > tensor3D_t;
typedef std::vector<std::vector<gi::ex> > slab_t;
typedef std::vector<gi::ex> line_t;

typedef std::vector<std::vector<int> > matrix_int_t;
typedef std::vector<int> vector_int_t;

/*******************************************************************************
 *                                Prototypes                                   *
 *******************************************************************************/

// sequential
gi::ex multiply_seq( tensor3D_t&, int );
gi::ex multiply_1level( tensor3D_t&, int );
gi::ex multiply_2levels( tensor3D_t&, int );
// parallel
gi::ex multiply_1level_mw( tensor3D_t&, int );
gi::ex multiply_1level_mw2( tensor3D_t&, int );
gi::ex multiply_1level_mw3( tensor3D_t&, int );
gi::ex multiply_1level_mw_addslave( tensor3D_t&, int );
gi::ex multiply_1level_mw_addslave2( tensor3D_t&, int );
gi::ex multiply_1level_mw_addslave3( tensor3D_t&, int );
gi::ex multiply_1level_mw_addslave4( tensor3D_t&, int );
gi::ex multiply_1level_mw_local( tensor3D_t&, int );
gi::ex multiply_2levels_mw_hierarch( tensor3D_t&, int );
gi::ex multiply_2levels_mw_hierarch2( tensor3D_t&, int );
gi::ex multiply_combined( tensor3D_t&, int );

/*******************************************************************************
 *                              Default values                                 *
 *******************************************************************************/

#define DEFAULTN           4    /* Default size */
#define DEFAULTFUNCTION   'm'   /* Default function to use */
#define NBFOREMEN          2    /* Default number of foremen to use with the hierarchical M/W */

#define ROOT 0

#define MAXRESULT 8   /* Maximum results to accumulate before sending the addition to a slave */

#endif // ndef _TENSORMATRIX_H_
