#include <iostream>
#include <mpi.h>
#include <numeric>
#include <ginac/ginac.h>

#include "products.h"
#include "utils_parall.h"
#include "parall_constants.h"
#include "parall_internal.h"
#include "utils.h"
#include "profiling.h"

namespace gi = GiNaC;

typedef enum {
	      LONG_ADD_M,  /* The addition on the master took a long time */
	      FINISHED,    /* Computation finished normally */
} end_code_t;

typedef enum {
	      ALGO_MW,    /* Regulat master-worker */
	      ALGO_ADDSLAVE,   /* Do the addition on a slave */
} algo_t;

/* This one is a "regular" master. It returns either when it is done, or when it decides to switch to another algorithm.
 */

end_code_t multiply_combined_master_initial( tensor3D_t& T, unsigned int size, gi::ex& Tens, MPI_Comm comm = MPI_COMM_WORLD ) { 
    unsigned int a1, a2, a4;
    gi::ex A;
    gi::lst symbols;

    MPI_Status status;
    char* expr_c;
    size_t expr_c_size = 0;
    int src, np, running = 0;
    unsigned int len;

    double t_start, t_add, t_wait, t_average;
    std::vector<double> times;
    int times_idx;
    
    algo_t algo = ALGO_MW;

    MPI_Comm_size( comm, &np );

    expr_c = NULL;
    expr_c = (char*) malloc( 3279 ); // TMP
    
    int i, j;
    i = 0;
    j = 0;

    int receivedresults = 0;
    unsigned int N = size/2;

    std::vector<parameters_t> input;
    std::vector<std::string> results_s;
    std::vector<gi::ex> results;

    /* Build a list of argument sets */
    
    for( a4 = 0 ; a4 < N ; a4++ ){
        i=i+1; 
        for( a2 = 0; a2 < N ; a2++ ){
            j=j+1; 
            for( a1 = 0 ; a1 < N ; a1++ ){
                parameters_t p( a4, a2, a1 );
                input.push_back( p );
            }
	    }
	}
   
    /* Compute the set of symbols */
    /* Could be done while the first slave is working */
    
    symbols = all_symbols_3D( size );

    /* Workers that have yet to send their first request */
    
    bool initialround = true;
    running = 0;
    for( i = 0 ; i < np - 1 ; i++ )
	times.push_back( 0.0 );
    times_idx = 0;
    t_average = 0.0;
    
    /* Distribute the work */

    while( input.size() > 0 ) {
        t_start = rdtsc();
        MPI_Recv( &len, 1, MPI_UNSIGNED, MPI_ANY_SOURCE, MPI_ANY_TAG, comm, &status );
        src = status.MPI_SOURCE;
        t_wait = rdtsc() - t_start;
        times[ times_idx] = t_wait;
        times_idx = ( times_idx + 1 ) % ( np - 1 );
        if( !initialround )
            t_average = std::accumulate( times.begin(), times.end(), 0.0 )/(double)(np - 1);
        
        if( status.MPI_TAG == TAG_PULL ) {
            
            /* Nothing else will come: just send wome work */
            send_work( input, src, comm );
            
            if( initialround ){
                running++;
                if( np - 1 == running ) initialround = false; // everyone is at work
            }
            
        } else {
            if( status.MPI_TAG == TAG_RES ){
                src = status.MPI_SOURCE;
                
                /* The first message contains the length of what is coming next */
                if( len != 0 ) {
                    if( len > expr_c_size ) {
                        expr_c_size = len;
                        if( NULL != expr_c ) free( expr_c );
                        expr_c = (char*)malloc( expr_c_size ); // The \0 was added by the slave
                    }
                    
                    /* Receive the result */
                    MPI_Recv( expr_c, len, MPI_CHAR, src, TAG_EXPR, comm, &status );
                    
                    /* put it in the result queue */
                    std::string s( expr_c );
                    
                    if( algo == ALGO_MW) {
                        send_work( input, src, comm );
                    }
                    
                    /* Process what I have just received */
                    
                    if( ALGO_MW == algo ) {
                        t_start = rdtsc();
                        gi::ex received = de_linearize_expression( s, symbols );
                        Tens += received;
                        t_add = rdtsc() - t_start;
                        std::cout << "Add " << t_add << std::endl;
#if DEBUG
                        results.push_back( received );
                        results_s.push_back( s );
                        receivedresults++;
#endif
                        
                        if( !initialround && t_add > t_average ) {
                            /* We are spending too much time adding these results. Now we are going to ask a worker to do this. */
                            std::cout << "The master spent too much time computing the sum. Switch to ADDSLAVE algorithm" << std::endl;
                            algo = ALGO_ADDSLAVE;
                        }
                    } else {
                        if( ALGO_ADDSLAVE == algo ) {
                            results_s.push_back( s );
                            send_work_addslave( input, results_s, src );
                        } else {
                            std::cout << "ERROR: unknown algorithm on the master " << algo << std::endl;
                        }
                    }
                } else {
                    /* Send more work  */
                    send_work( input, src, comm );
                    
                }                
            } else{
                std::cerr << "Wrong tag received " << status.MPI_TAG << std::endl;
            }
        }
   }
    
    /* Wait until everyone is done */
    
    running = np - 1; // all the slaves are running 
    while( running > 0 ) {

        MPI_Recv( &len, 1, MPI_UNSIGNED, MPI_ANY_SOURCE, MPI_ANY_TAG, comm, &status );
        src = status.MPI_SOURCE;
        
        if( len != 0 ) {
            if( len > expr_c_size ) {
                expr_c_size = len;
                if( NULL != expr_c ) free( expr_c );
                expr_c = (char*)malloc( expr_c_size ); // The \0 was added by the slave
            }
            
            /* Receive the result */
            MPI_Recv( expr_c, len, MPI_CHAR, src, TAG_EXPR, comm, &status );
            
            /* And send the END signal */
            send_end( src, comm );
            running--;
            
            /* Process what I have just received */
            /* Could be given to a slave... */
            /* put it in the result queue */
            std::string s( expr_c );
            gi::ex received = de_linearize_expression( s, symbols );
            Tens += received;
#if DEBUG
            results.push_back( received );
            results_s.push_back( s );
            receivedresults++;
#endif
        } else {
            send_end( src, comm );
            running--;
        }
    }
    
#if DEBUG
    std::cout << "Received " << receivedresults << " results" << std::endl;

    std::cout << "Tpara=" << Tens << ";" << std::endl;
#endif
    
    if( NULL != expr_c) free( expr_c );
    return FINISHED;
}

/* The traditional slave */

void multiply_combined_slave_initial( tensor3D_t& T, int size, MPI_Comm comm = MPI_COMM_WORLD ) {
    gi::ex Tens;
    int  a1, a2, a4;
    //    gi::ex A;
    unsigned int len = 0;
    
    parameters_t params;
    MPI_Status status;
    char* expr_c;

    double t_start, t_wait, t_compute;

    int rank;
    MPI_Comm_rank( comm, &rank );
    
    /* Ask for some work */
    
    MPI_Send( &len, 1, MPI_UNSIGNED, ROOT, TAG_PULL, comm );

    /* Compute the set of symbols */
    
    gi::lst symbols = all_symbols_3D( size );

    while( true ){
        /* Receive a set of parameters */
        
        t_start = rdtsc();
        MPI_Recv( &params, 1, DT_PARAMETERS, ROOT, MPI_ANY_TAG, comm, &status );
        t_wait = rdtsc() - t_start;
        
        if( status.MPI_TAG == TAG_WORK ){
            a1 = params.a1;
            a2 = params.a2;
            a4 = params.a4;
            
            t_start = rdtsc();
            Tens = one_level1_product( &T, size, a4, a2, a1 );
            t_compute = rdtsc() - t_start;
            
	    /* TODO if we waited for too long */
	    if( t_wait > t_compute ) {}

            send_result( Tens );

        } else {
            if( status.MPI_TAG == TAG_ADD ) {
                /* Receive a set of expressions to add */
		
                /* Number of expressions received */
                int nb = params.a4;
                
                /* Length of each string */
		
                unsigned int* lengths = (unsigned int*) malloc( nb*sizeof( unsigned int ) );
                MPI_Recv( lengths, nb, MPI_INT, ROOT, TAG_ADD, comm, &status );
                std::vector<std::string> results_s;
                char* c_str;
                int i;
                int len;
                for( i = 0 ; i < nb ; i++ ) {
                    len = lengths[i] + 1;
                    c_str = (char*) malloc( len );
                    MPI_Recv( c_str, len, MPI_CHAR, ROOT, TAG_ADD, comm, &status );
                    c_str[len-1] = '\0';    // The master sends C++ strings, which do not contain the final '\0'
                    results_s.push_back( std::string( c_str ) );
                    free( c_str );
                }
                
                /* Delinearize all the expressions and add them */
                
                Tens = add_expressions( results_s, symbols );
                
                /* Send the result */
                
                send_result( Tens );
                
            } else {
                if( status.MPI_TAG == TAG_END ){
                    return;
                } else {
		    std::cerr << "Wrong tag received on slave " << status.MPI_TAG << std::endl;
                }
            }
        }
    }
}

/*******************************************************************************
 *                         Combined master-worker                              *
 *******************************************************************************/

gi::ex multiply_combined_master( tensor3D_t& T, int size ) {  // simpler: same dimension everywhere
    gi::ex Tens = 0;
    end_code_t rc;
    
    /* Initially: start as a traditional M/W */
    
    rc = multiply_combined_master_initial( T, size, Tens );
    switch( rc ){
    case FINISHED:
        return Tens;
    }
    
    return Tens;
}

void multiply_combined_worker( tensor3D_t& T, int size ) {  // simpler: same dimension everywhere
    gi::ex Tens = 0;
    
    std::cout << "worker" << std::endl;
    multiply_combined_slave_initial( T, size );
    
}


gi::ex multiply_combined( tensor3D_t& T, int size ) {  // simpler: same dimension everywhere

    int rank;
    gi::ex Tens = 0;
    MPI_Comm_rank( MPI_COMM_WORLD, &rank );

    /* Create a new datatype for the parameters */
    
    create_parameters_datatype();

    /* Here we go */
    
    if( 0 == rank ) {
        Tens = multiply_combined_master( T, size );
    } else {
        multiply_combined_worker( T, size );
    }

    /* Finalize */
    
    free_parameters_dt();
    return Tens;

}
