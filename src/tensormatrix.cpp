#include <iostream>
#include <cstdlib>
#include <ginac/ginac.h>

#define N 4

/* GiNaC is not thread-safe -> no OpenMP */

namespace gi = GiNaC;

typedef std::vector<std::vector<std::vector<gi::ex> > > tensor3D_t;
typedef std::vector<std::vector<gi::ex> > slab_t;
typedef std::vector<gi::ex> line_t;

typedef std::vector<std::vector<int> > matrix_int_t;
typedef std::vector<int> vector_int_t;

/*
  libginac-dev ginac-tools
  g++ -std=c++11 -O3 -Wall -o tensormatrix tensormatrix.cpp -lginac
*/
/* Sequentiel sur Minimum
real	3m31,034s
*/

void create_tensor_3D( tensor3D_t&, int, int, int );
void print_tensor_3D( tensor3D_t&, int, int, int );

void init_simplectic( matrix_int_t&, int, int );
void print_matrix( matrix_int_t&, int, int );

gi::ex multiply( tensor3D_t&, matrix_int_t&, int );

/* StarPU codelets */

void inner_product( void *buffers[], void *cl_arg ){

}

int main(){

    tensor3D_t T;
    matrix_int_t J;
    
    gi::ex Tens = 0;
    
    /* Initialize the tensor with symbolic values */
    
    create_tensor_3D( T, N, N, N );

    /* Initialize the simplectic matrix */

    init_simplectic( J, N, N );

    /* Check */
    
    /*   print_tensor_3D( T, N, N, N );
         print_matrix( J, N, N ); */

    /* Here comes the fun */

    Tens = multiply( T, J, N );

    std::cout << "Tens: " << Tens << std::endl;

    return EXIT_SUCCESS;
}

/* 
 * Fill the tensor 
 * This function is pretty specialized, because it works for 3D tensors only.
 * If we need something more general, I will write it.
*/

void create_tensor_3D( tensor3D_t& tensor, int size1, int size2, int size3 ) {
    for( auto i = 0 ; i < size3 ; i++ ){
        slab_t slab;
        for( auto j = 0 ; j < size2 ; j++ ){
            std::vector<gi::ex> line;
            for( auto k = 0 ; k < size1 ; k++ ) {
                std::string name = "T_" + std::to_string( i ) + std::to_string( j ) + std::to_string( k );
                gi::symbol toto( name );
                line.push_back( toto );
            }
            slab.push_back( line );
        }
        tensor.push_back( slab );
    }
}

/*
 * Output a tensor
 * This function is pretty specialized, because it works for 3D tensors only.
 * If we need something more general, I will write it.
*/

void print_tensor_3D( tensor3D_t& tensor, int size1, int size2, int size3 ) {
    for( auto i = 0 ; i < size3 ; i++ ){
        std::cout << " - Slab " << i << std::endl;
        for( auto j = 0 ; j < size2 ; j++ ){
            for( auto k = 0 ; k < size1 ; k++ ) {
                std::cout << tensor[i][j][k] << "  ";
            }
            std::cout << std::endl;
        }
        std::cout << " ---- " << std::endl;
    }
}

/* 
 * Initialize the simplectic matrix
 */

void init_simplectic( matrix_int_t& mat, int lines, int columns ) {
   
    for( auto i = 0 ; i < lines/2 ; i++ ){
        vector_int_t line( N );
        line[columns/2 + i] = 1;
        mat.push_back( line );
    }
    for( auto i = 0 ; i < lines/2 ; i++ ){
        vector_int_t line( N );
        line[i] = -1;
        mat.push_back( line );
    }

}

void print_matrix( matrix_int_t& mat, int lines, int columns ) {
    for( auto i = 0 ; i < lines ; i++ ){
        for( auto j = 0 ; j < columns ; j++ ){
            std::cout << mat[i][j] << " ";
        }
        std::cout << std::endl;
    }

}

gi::ex multiply( tensor3D_t& T, matrix_int_t& J, int size ) {  // simpler: same dimension everywhere

    gi::ex Tens = 0;
	int a1, a2, a3, b1, b2, b3, c1, c2, c3, d1, d2, d3;
    gi::ex TAB, TABB, TABC, TABCC, TABCD, TABCDD; 
    gi::ex A;

 	int i, j;
    i = 0;
    j = 0;

   for( a1 = 0 ; a1 < size; a1++ ){
		i=i+1; 
        std::cout << "Tens: " << Tens << std::endl;
		printf("i = %d\n", i); 
		for( a2 = 0; a2 < size ; a2++ ){
			j=j+1; 
			printf("j = %d\n", j);
			for( a3 = 0 ; a3 < size ; a3++ ){
				A = T[a1][a2][a3];
                /* Beyond this point, a2 and a3 are only used in the simplectic matrix */
				for( b1 = 0 ; b1 < size ; b1++ ){
					TAB = J[a1][b1]; 
					for( b2 = 0 ; b2 < size ; b2++ ){
						for( b3 = 0 ; b3 < size ; b3++ ){
							TABB =  TAB * A*T[b1][b2][b3];
                           /* Beyond this point, b1 is not used anymore */
							for( c1 = 0 ; c1 < size ; c1++ ){
								for( c2 = 0 ; c2 < size ; c2++ ){
									TABC = TABB * J[a2][c2];
									for( c3 = 0 ; c3 < size ; c3++ ){
										TABCC = TABC * T[c1][c2][c3] * J[b3][c3] ; 
										for( d1 = 0 ; d1 < size ; d1++ ){
											TABCD = TABCC * J[c1][d1];
											for( d2 = 0 ; d2 < size ; d2++ ){
												TABCDD = TABCD * J[b2][d2];
												for( d3 = 0 ; d3 < size ; d3++ ){
													Tens = Tens + TABCDD * T[d1][d2][d3]*J[a3][d3];
												}
                                            }
										}
									}
								}
							}
						}
                    }
				}
			}
		}
	}
    return Tens;
}
