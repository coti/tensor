#include <iostream>
#include <mpi.h>
#include <ginac/ginac.h>

#include "products.h"
#include "utils_parall.h"
#include "parall_constants.h"
#include "parall_internal.h"
#include "utils.h"
#include "profiling.h"

namespace gi = GiNaC;

/*******************************************************************************
 *                     Parallel 1-level decomposition.                         *
 *        Each worker keeps its result. Add them on a tree at the end          *
 *******************************************************************************/

gi::ex add_tree( gi::ex mine, gi::lst symbols,  MPI_Comm comm = MPI_COMM_WORLD ){

    int rank, size, step, buddy;
    gi::ex Tens = mine;

    MPI_Comm_size( comm, &size );
    MPI_Comm_rank( comm, &rank );

    std::vector<double> times;
    
    step = 0;
    if( 0 == rank ) times.push_back( getTime() );
    
    while( ( 0x1 << step ) < size ) {
        if( 0 == ( rank % ( 0x1 << ( step + 1 ) ) ) )  {

            /* even number: I am a receiver */
            
            buddy = rank + (0x1 << step );

            if( buddy < size ) {
                /* receive the result and add it */
                Tens += recv_result( buddy, symbols, comm );
		if( 0 == rank ) times.push_back( getTime() );
            }
        } else {
            
            /* odd number: I am a sender */
            
            buddy = rank - (0x1 << step );
            
            send_result( Tens, buddy, comm );
            return Tens; /* I have sent my result, I am done here */
        }
        step++;
    }

    if( 0 == rank ) {
      for( auto t: times ){
	std::cout << t - times[0] << "\t" ;
      }
      std::cout << std::endl;
    }

    return Tens;
}


gi::ex multiply_1level_master_local( tensor3D_t& T, unsigned int size, MPI_Comm comm = MPI_COMM_WORLD ) { 
    gi::ex Tens = 0;
    unsigned int a2, a4;
    gi::ex A;
    gi::lst symbols;

    MPI_Status status;
    char* expr_c;
    size_t expr_c_size = 0;
    int src, np, running = 0;
    unsigned int len;
    parameters_2_1_t pzero( 0, 0 );

    MPI_Comm_size( comm, &np );

    expr_c = NULL;
    expr_c = (char*) malloc( 3279 );
    
    int receivedresults = 0;
    unsigned int N = size/2;

    std::vector<parameters_2_1_t> input;
    std::vector<std::string> results; /* length and char* */

     double t1 = getTime();

   /* Build a list of argument sets */
    
    for( a4 = 0 ; a4 < N ; a4++ ){
        for( a2 = 0; a2 < N ; a2++ ){
            parameters_2_1_t p( a4, a2 );
            input.push_back( p );
	    }
	}

    /* Compute the set of symbols */
    /* Could be done while the first slave is working */
    
    symbols = all_symbols_3D( size );

    double t2 = getTime();
    
    /* Distribute the work */

    while( input.size() > 0 ) {
        MPI_Recv( &len, 1, MPI_UNSIGNED, MPI_ANY_SOURCE, TAG_PULL, comm, &status );
        
        /* Nothing else will come: just send wome work */
        src = status.MPI_SOURCE;
        send_work( input, src );

   }

    double t3 = getTime();

   /* Wait until everyone is done */

    running = np - 1; // all the slaves are running 
    while( running > 0 ) {
      MPI_Recv( &len, 1, MPI_UNSIGNED, MPI_ANY_SOURCE, TAG_PULL, comm, &status );
      src = status.MPI_SOURCE;

      send_end( src, pzero, comm );
      running--;
    }

    double t4 = getTime();

    /* Take part of the tree */

    add_tree( Tens, symbols, comm );
    
    double t5 = getTime();
    std::cout << t2 - t1;
    std::cout << "\t " << t3 - t2;
    std::cout << "\t " << t4 - t3;
    std::cout << "\t " << t5 - t4 << std::endl;

#if DEBUG
    std::cout << "Received " << receivedresults << " results" << std::endl;

    std::cout << "Tpara=" << Tens << ";" << std::endl;
#endif
    
    if( NULL != expr_c) free( expr_c );
    return Tens;
}

void multiply_1level_slave_local( tensor3D_t& T, unsigned int size, MPI_Comm comm = MPI_COMM_WORLD ) {
    gi::ex Tens = 0;
    int  a2, a4;
    unsigned int len = 0;
    
    parameters_2_1_t params;
    MPI_Status status;
    char* expr_c;
    MPI_Request req;

    std::vector<gi::ex> results;

    int rank;
    MPI_Comm_rank( comm, &rank );

    /* Ask for some work */
    
    MPI_Send( &len, 1, MPI_UNSIGNED, ROOT, TAG_PULL, comm );

    /* Compute the set of symbols */
    
    gi::lst symbols = all_symbols_3D( size );

    while( true ){
        /* Receive a set of parameters */

        MPI_Irecv( &params, 1, DT_PARAMETERS_2_1, ROOT, MPI_ANY_TAG, comm, &req );
        if( results.size() >= 1 ) {
            for( auto e: results ) {
                Tens += e;
            }
            results.clear();
        }
        MPI_Wait( &req, &status );

        /* TODO overlap with an addition */
        
        if( status.MPI_TAG == TAG_WORK ){
            a4 = params.a4;
            a2 = params.a2;

            Tens = one_level1_product( &T, size, a4, a2 );
            results.push_back( Tens );
            
            MPI_Send( &len, 1, MPI_UNSIGNED, ROOT, TAG_PULL, comm );

        } else {
            if( status.MPI_TAG == TAG_END ){

                Tens = add_tree( Tens, symbols, comm );                
                return;
            } else {
                std::cerr << "Wrong tag received on slave " << status.MPI_TAG << std::endl;
            }
        }
    }
}

/* Communication protocol:
   M -> W: always the same size, therefore unique communication
   W -> M: send an unsigned int (size of the expression), then the expression (table of chars)
*/
        
gi::ex multiply_1level_mw_local( tensor3D_t& T, int size ) {  // simpler: same dimension everywhere
    int rank;
    gi::ex Tens = 0;
    MPI_Comm_rank( MPI_COMM_WORLD, &rank );

    /* Create a new datatype for the parameters */
    
    create_parameters_datatype_2_1();

    /* Here we go */

    if( 0 == rank ) {
        Tens = multiply_1level_master_local( T, size );
    } else {
        multiply_1level_slave_local( T, size );
    }

    /* Finalize */
    
    free_parameters_2_1_dt();
    return Tens;
}

