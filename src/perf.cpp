#include <time.h>

/*******************************************************************************
 *                          Performance measurement                            *
 *******************************************************************************/

double getTime(){
#if PAPI
    return PAPI_get_real_usec();
#else
    struct timespec ts;
    clock_gettime( CLOCK_REALTIME, &ts );
    return ( ts.tv_nsec + ts.tv_sec * 1000000000 );
#endif
}

