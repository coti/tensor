#ifndef _UTILITIES_H_
#define _UTILITIES_H_

void create_tensor_3D( tensor3D_t&, int, int, int );
void print_tensor_3D( tensor3D_t&, int, int, int );
void init_simplectic( matrix_int_t&, int, int );
void print_matrix( matrix_int_t&, int, int );

void store_expression( std::string, gi::ex, std::string );
void load_expression( std::string, gi::ex&, std::string, gi::lst );
gi::lst get_symbols( gi::ex& );
gi::lst all_symbols_3D( int );

double getTime( void );
void help( void );

#endif // ndef _UTILITIES_H_
