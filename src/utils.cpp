#include <iostream>
#include <ginac/ginac.h>
#include <fstream> // debug?

#include "tensormatrix.h"

namespace gi = GiNaC;

/*******************************************************************************
 *                             Basic utilities                                 *
 *******************************************************************************/

/* 
 * Fill the tensor 
 * This function is pretty specialized, because it works for 3D tensors only.
 * If we need something more general, I will write it.
*/

void create_tensor_3D( tensor3D_t& tensor, int size1, int size2, int size3 ) {
    for( auto i = 0 ; i < size3 ; i++ ){
        slab_t slab;
        for( auto j = 0 ; j < size2 ; j++ ){
            std::vector<gi::ex> line;
            for( auto k = 0 ; k < size1 ; k++ ) {
                std::string name = "T_" + std::to_string( i ) + std::to_string( j ) + std::to_string( k );
                gi::symbol toto( name );
                line.push_back( toto );
            }
            slab.push_back( line );
        }
        tensor.push_back( slab );
    }
}

/*
 * Output a tensor
 * This function is pretty specialized, because it works for 3D tensors only.
 * If we need something more general, I will write it.
*/

void print_tensor_3D( tensor3D_t& tensor, int size1, int size2, int size3 ) {
    for( auto i = 0 ; i < size3 ; i++ ){
        std::cout << " - Slab " << i << std::endl;
        for( auto j = 0 ; j < size2 ; j++ ){
            for( auto k = 0 ; k < size1 ; k++ ) {
                std::cout << tensor[i][j][k] << "  ";
            }
            std::cout << std::endl;
        }
        std::cout << " ---- " << std::endl;
    }
}

/* 
 * Initialize the simplectic matrix
 */

void init_simplectic( matrix_int_t& mat, int lines, int columns ) {
   
    for( auto i = 0 ; i < lines/2 ; i++ ){
        vector_int_t line( columns );
        line[columns/2 + i] = 1;
        mat.push_back( line );
    }
    for( auto i = 0 ; i < lines/2 ; i++ ){
        vector_int_t line( columns );
        line[i] = -1;
        mat.push_back( line );
    }

}

void print_matrix( matrix_int_t& mat, int lines, int columns ) {
    for( auto i = 0 ; i < lines ; i++ ){
        for( auto j = 0 ; j < columns ; j++ ){
            std::cout << mat[i][j] << " ";
        }
        std::cout << std::endl;
    }

}

gi::lst get_symbols( gi::ex& expression ){
    gi::lst symbols; /* Get the list of symbols used in Tens */
    for( const auto& s: expression ){
        symbols.append( s );
    }
    return symbols;
}

gi::lst all_symbols_3D( int size ){
    gi::lst symbols;
    for( auto i = 0 ; i < size ; i++ ){
        for( auto j = 0 ; j < size ; j++ ){
            for( auto k = 0 ; k < size ; k++ ){
                std::string name = "T_" + std::to_string( i ) + std::to_string( j ) + std::to_string( k );
                gi::symbol toto( name );
                symbols.append( toto );
            }
        }
    }
    return symbols;
}

void store_expression( std::string filename, gi::ex expression, std::string name ){
    gi::archive a;
    a.archive_ex( expression, name.c_str() );
    std::ofstream out( filename );
    out << a;
    out.close();
}

void load_expression( std::string filename, gi::ex& expression, std::string name, gi::lst symbols ){
    gi::archive a;
    std::ifstream in( filename );
    in >> a;
    expression = a.unarchive_ex( symbols, name.c_str() );
}

void help(){
    std::cout << "Arguments:" << std::endl;
    std::cout << "(mpiexec .... ) ./tensormatrix_mpi [N] [Function name] [Nb of foremen] : run the program" << std::endl;
    std::cout << "(mpiexec .... ) ./tensormatrix_mpi h                                   : display help" << std::endl;
    std::cout << "Function names being: " << std::endl;
    std::cout << " - M/m: Master-Worker -> multiply_1level_mw" << std::endl;
    std::cout << " - A/a: Master-Worker, addition on a slave -> multiply_1level_mw_addslave" << std::endl;
    std::cout << " - H/h: Hierarchical master-worker -> multiply_1level_mw_hierarch" << std::endl;
    std::cout << " - S/s: Sequential -> multiply_seq" << std::endl;
    std::cout << " - 1  : Sequential, 1 level of decomposition -> multiply_1level" << std::endl;
    std::cout << " - 2  : Sequential, 2 levels of decomposition -> multiply_2levels" << std::endl;
}

