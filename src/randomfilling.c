#include <stdlib.h>
#include <stdio.h>
#include <time.h>
//**************************************

# define N 8




void afficher(int* M, int p, int q);


int main ()
{
	srand(time(NULL)); // initialise le gÃ©nÃ©rateur alÃ©atoire
	
	int* T = malloc(N*N*N*sizeof(int)); // tenseur sous forme de vecteur
	int* J = malloc(N*N*sizeof(int)); 
	int i,j,k;
	int a1,a2,a3,b1,b2,b3,c1,c2,c3,d1,d2,d3; 
	int A, TAB, TABB, TABC, TABCC, TABCD, TABCDD; 
	int Tens; 


	// remplissage de la matrice simplectic 8*8, 	J = (0, I_N, -I_N, 0) 

	for (i=0; i<N; i++)
		for (j=0; j<N; j++){
			if (i<4 && j<4)
				J[i*N+j] =0 ; 
			if (j>= 4 && j == 4+i)
				J[i*N+j] = 1;
			else if (j>= 4 )
					J[i*N+j] = 0; 
			if (i>=4 && j>=4)
				J[i*N+j] =0 ; 
			if (j< 4 && j == i-4)
				J[i*N+j] = -1;
		}
	afficher(J,N,N); 


	// remplissage du tenseur avec des nombres aleatoires entre 0 et 50 

	for (i=0; i<N; i++)
		for (j=0; j<N; j++)
			for (k=0; k<N; k++)
				T[i*N*N +j*N + k] = rand() % 50 ;


	// Check 

	printf("T[%d,%d,%d] = %d\n",i-1,j-1,k-1,T[(i-1)*N*N+(j-1)*N + k-1]); 
  afficher(T,N,N); 
  printf("\n");


// Ici, la vraie histoire commence
	Tens =0 ;  		
	i =0; 
	

	for (a1=0; a1<N; a1++){
		i=i+1; 
		printf("Tens = %d\n", Tens); 
		printf("i = %d\n", i); 
		j = 0; 
		for (a2 = 0; a2<N; a2++){
			j=j+1; 
			printf("j = %d\n", j);
			for (a3=0; a3<N; a3++){
				A = a1*N*N+a2*N+a3; 
				for (b1=0; b1<N; b1++){
					TAB = J[a1*N+b1]; 
					for (b2=0; b2<N; b2++){
						for (b3=0; b3<N; b3++){
							TABB =  TAB * T[A]*T[b1*N*N+b2*N+b3]; 
							for (c1=0; c1<N; c1++){
								for (c2=0; c2<N; c2++){
									TABC = TABB * J[a2*N+c2];
									for (c3=0; c3<N; c3++){
										TABCC = TABC * T[c1*N*N+c2*N+c3] * J[b3*N+ c3] ; 
										for (d1=0; d1<N; d1++){
											TABCD = TABCC * J[c1*N + d1]; 
											for (d2=0; d2<N; d2++){
												TABCDD = TABCD * J[b2*N + d2];
												for (d3=0; d3<N; d3++){
													Tens = Tens + TABCDD * T[d1*N*N+d2*N+d3]*J[a3*N+d3];
												}
											}
										}
									}
								}
							}
						}
					}
				}
			}
		}
	} 

	printf("Tens = %d\n", Tens); 
 
	free(T);
	free(J);
	 
 	return EXIT_SUCCESS;
}



void afficher(int* M, int p, int q)
{
 int i,j;
 printf("\n");
 for (i=0; i<p; i++)
 {
  for (j=0; j<q; j++)
  {
   printf("%d ",M[i*q+j]);
  }
  printf("\n");
 }
}
