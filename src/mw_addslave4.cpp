#include <iostream>
#include <mpi.h>
#include <ginac/ginac.h>
#include <math.h> // for ceil

#include "products.h"
#include "utils_parall.h"
#include "parall_constants.h"
#include "parall_internal.h"
#include "utils.h"
#include "profiling.h"

#include <unistd.h>

namespace gi = GiNaC;

#define MAXLENADD  1 // 256

unsigned int maxlen(  std::vector<std::string> expressions ){
    unsigned int len = 0;
    for( auto s: expressions  ) {
        unsigned int l2 = s.length();
        if( len < l2 ) {
            len = l2;
        }
    }
    return len;
}

#ifdef SCATTERGATHER


gi::ex add_expressions_parall( std::vector<std::string> expressions, gi::lst symbols, parameters_2_1_t p, MPI_Comm comm = MPI_COMM_WORLD ) {

    gi::ex Tens = 0;
    int i, peer, nb, len;
    int rank, size;
    int chunk, end;
    char* expr_c;
    char* expr_2;
    char* tmp;
    char* total_c = NULL;
    int expr = 0;
    int* m_len;
    int* m_disp;
    char* toto = NULL;
    int totallen = 0;
    MPI_Request req;
    MPI_Status stat;

    MPI_Comm_rank( comm, &rank );
    MPI_Comm_size( comm, &size );

    if( 0 == rank ) {
        /* If the expressions are short, compute the sum locally */
        if( maxlen( expressions ) < MAXLENADD )
            return add_expressions( expressions, symbols );
        
        nb = expressions.size();

    }
    
    /* Broadcast the number of expressions to add */
    
    MPI_Bcast( &nb, 1, MPI_UNSIGNED, 0, comm );
    
    int* lengths = ( int*) malloc( size * nb * sizeof( int ) );
    int* displ   = ( int*) malloc( size * nb * sizeof( int ) );

    m_len   = ( int*) malloc( size * sizeof( int ) );
    m_disp  = ( int*) malloc( size * sizeof( int ) );
   
    /* Send all the number of elements and displacements, grouped by peer */
        
    if( 0 == rank ) {
        
       for( auto s: expressions  ) {
            chunk = ceil( s.length() / size );

            for( peer = 0 ; peer < size ; peer++ ) {

                if( 0 == peer ) {
                    displ[ peer * nb + expr] = 0;
                    end = chunk;
                } else {
                    displ[ peer * nb + expr] = end + 1;
                    end = displ[ peer * nb + expr] + chunk;
                }
                /* How much are we going to send: stop at a + or - sign (and keep the sign) */
                
                while( ( end < (int)s.length() - 1) && !( s[end] == '+' || s[end] == '-' || end == (int)s.length() - 1 )) {
                    end++;
                }
                end--;

                if( 0 == peer ) {
                    lengths[ expr ] = end + 1;
                } else {
                    lengths[ peer * nb + expr ] = end - displ[ peer  * nb + expr ] + 1;
                }
            }
            expr++;
        }
    }

    MPI_Scatter( lengths, nb, MPI_INT, lengths, nb, MPI_INT, 0, comm );
    MPI_Scatter( displ,   nb, MPI_INT, displ,   nb, MPI_INT, 0, comm );

    /* Allocate the reception buffer */

    int maxlen = 0;
    std::vector<std::string> results_s;

    for( expr = 0 ; expr < nb ; expr++ ) {
        maxlen = ( maxlen < lengths[ expr ] ) ? lengths[ expr ] : maxlen;
    }
    expr_c = (char*)malloc( maxlen + 1 ); // Add a final \0
    expr_2 = (char*)malloc( maxlen + 1 ); 

    /* Send the expressions */

    for( expr = 0 ; expr < nb ; expr++ ) {

        len = lengths[expr]; //  correct even for rank == 0
        if( 0 == rank ) {
            for( peer = 0 ; peer < size ; peer++ ) {
                m_disp[ peer ] = displ[ peer * nb + expr];
                m_len[ peer ]  = lengths[ peer * nb + expr];
            }
        }
        
        if( 0 == rank ) toto = const_cast<char*>( expressions[expr].c_str() );

        MPI_Iscatterv( toto, m_len, m_disp, MPI_CHAR,
                      expr_c, len, MPI_CHAR,
                      0, comm , &req );

        if( 0 != expr ) {
            Tens += de_linearize_expression( std::string( expr_2 ), symbols );
        }
        
        /* get the new expression in expr_c and swap the pointers so that we can work with expr_2 and receive in expr_c */
        MPI_Wait( &req, &stat );
        expr_c[len ] = '\0';    // The master sends C++ strings, which do not contain the final '\0'
        tmp = expr_2;
        expr_2 = expr_c;
        expr_c = tmp;
        
    }
    Tens += de_linearize_expression( std::string( expr_2 ), symbols );
    
    /* Send the result to the master */

    std::string expr_s = linearize_expression( Tens );
    len = expr_s.length();
    
    MPI_Gather( &len, 1, MPI_INT, m_len, 1, MPI_INT, 0, comm );

    if( 0 == rank ) {
        for( peer = 0 ; peer < size ; peer++ ){
            m_disp[peer] = totallen + 1;
            totallen += m_len[peer];
        }
        m_disp[0] = 0;
            
        total_c = (char*) malloc( ( totallen + size ) * sizeof( char ) );
    }

    expr_c = const_cast<char*>( expr_s.c_str() );
    MPI_Gatherv( expr_c, len, MPI_CHAR, total_c, m_len, m_disp, MPI_CHAR, 0, comm );

    if( 0 == rank ){ /* replace the \n's by + */
        for( peer = 1 ; peer < size ; peer++ ){
            total_c[ m_disp[peer] - 1 ] = '+' ;
        }
        expr_c[ totallen + size - 1 ] = '\n' ;
    }

    //    Tens = de_linearize_expression( std::string( expr_c ), symbols );
    
    free( lengths );
    free( displ );
    if( 0 == rank ) {
        free( m_len );
        free( m_disp );
        free( total_c );
    }
    return Tens;
}


#else
gi::ex add_expressions_parall( std::vector<std::string> expressions, gi::lst symbols, parameters_2_1_t p, MPI_Comm comm = MPI_COMM_WORLD ) {
    gi::ex Tens = 0;
    int size, i, nb, len;
    unsigned int chunk, end;
    std::vector<unsigned int> cut;
    unsigned int* lengths;
    std::string result;
    char* expr;
    MPI_Status status;
    size_t expr_c_size = 0;
    char* expr_c;

    /* If the expressions are short, compute the sum locally */
    if( maxlen( expressions ) < MAXLENADD )
        return add_expressions( expressions, symbols );
    
    MPI_Comm_size( comm, &size );
    nb = expressions.size();
    lengths = (unsigned int*) malloc( nb * sizeof( unsigned int ) );
    for( i = 0 ; i < nb ; i++ ) {
        cut.push_back( 0 );
        lengths[i] = 0;
    }
    unsigned int running = size - 1;
    p.setParams( nb, 1 );

    /* TODO ca se factorise avec send_expressions_to_add */

    for( int peer = 1 ; peer < size ; peer++ ) {
    
        i = 0;
        for( auto s: expressions  ) {
            /* How much are we going to send: stop at a + or - sign (and keep the sign) */
            chunk = ceil( s.length() / ( size - 1 ) );
            end = cut[i] + chunk;
	    if( end >  ( s.length() - 1 ) ) {
	      end = s.length() - 1;
	    } else {
	      while( ( end != s.length() - 1) && !( s[end] == '+' || s[end] == '-' || end == s.length() - 1) ){
                end++;
	      }
	      end--;
	    }
	    
            lengths[i] = end - cut[i] + 1;
            i++;
        }
        
        /* Send the lengths */
        MPI_Send( &p, 1, DT_PARAMETERS_2_1, peer, TAG_ADD, comm );
        MPI_Send( lengths, nb, MPI_INT, peer, TAG_ADD, comm );
        
        /* Send the strings */
        
        for( unsigned int j = 0 ; j < nb ; j++ ) {
            expr = const_cast<char*>( expressions[j].c_str() );
        
           MPI_Send( &( expr[ cut[j] ] ), lengths[j], MPI_CHAR, peer, TAG_ADD, comm );
           cut[j] += lengths[j];
        }
    }
    
    /* Receive the results */
    
    expr_c = NULL;

    while( running > 0 ) {
        MPI_Recv( &len, 1, MPI_UNSIGNED, MPI_ANY_SOURCE, MPI_ANY_TAG, comm, &status );
        int src = status.MPI_SOURCE;
        len++;
        
        if( len != 0 ) {
            
            if( len > expr_c_size ) {
                expr_c_size = len;
                if( NULL != expr_c ) free( expr_c );
                expr_c = (char*)malloc( expr_c_size ); // The \0 was added by the slave
            }

            /* Receive the result */
            MPI_Recv( expr_c, len-1, MPI_CHAR, src, TAG_EXPR, comm, &status );
            expr_c[len - 1] = '\n';
            
            /* Concatenate the result */
            std::string recvs( expr_c );
            if( expr_c[0] != '-' ) result += '+';
            result += recvs;
        }
        running--;
        send_end( src, p );
    }
        
    Tens = de_linearize_expression( result, symbols );
            
    free( lengths );
    free( expr_c );
    return Tens;
}
#endif

/*******************************************************************************
 *         Parallel 1-level decomposition with addition on a slave             *
 *******************************************************************************/

gi::ex multiply_1level_master_addslave4( tensor3D_t& T, unsigned int size, MPI_Comm comm = MPI_COMM_WORLD ) { 
    gi::ex Tens = 0;
    unsigned int a2, a4;
    gi::lst symbols;

    MPI_Status status;
    char* expr_c;
    size_t expr_c_size = 0;
    int src, np;
    unsigned int len, running = 0;
    parameters_2_1_t pzero( 0, 0 );

    MPI_Comm_size( comm, &np );

    expr_c = NULL;
    expr_c = (char*) malloc( 3279 );
    
    int receivedresults = 0;
    unsigned int N = size/2;

    std::vector<parameters_2_1_t> input;
    std::vector<std::string> results; /* length and char* */

    double t1 = getTime();

    /* Build a list of argument sets */
    
    for( a4 = 0 ; a4 < N ; a4++ ){
        for( a2 = 0; a2 < N ; a2++ ){
            parameters_2_1_t p( a4, a2 );
            input.push_back( p );
	    }
	}

    /* Compute the set of symbols */
    /* Could be done while the first slave is working */
    
    symbols = all_symbols_3D( size );

    double t2 = getTime();

    /* Distribute the work */

    while( input.size() > 0 ) {
        MPI_Recv( &len, 1, MPI_UNSIGNED, MPI_ANY_SOURCE, MPI_ANY_TAG, comm, &status );
        
        if( status.MPI_TAG == TAG_PULL ) {
            /* Nothing else will come: just send wome work */
            src = status.MPI_SOURCE;
            send_work( input, src );
            
        } else {
            if( status.MPI_TAG == TAG_RES ){
                src = status.MPI_SOURCE;

                /* The first message contains the length of what is coming next */
                if( len != 0 ) {
                    if( len > expr_c_size ) {
                        expr_c_size = len;
                        if( NULL != expr_c ) free( expr_c );
                        expr_c = (char*)malloc( expr_c_size ); // The \0 was added by the slave
                    }
                    
                    /* Receive the result */
                    MPI_Recv( expr_c, len, MPI_CHAR, src, TAG_EXPR, comm, &status );

                    /* Put it in the result queue */
                    results.push_back( std::string( expr_c ) );
                }                    
                    
                /* Send more work  */
                send_work_addslave( input, results, src );
            } else {
                std::cerr << "Wrong tag received " << status.MPI_TAG << std::endl;
            }
            
        }
   }

    double t3 = getTime();

    /* Wait until everyone is done */

    running = np - 1; // all the slaves are running 
    while( running > 0 ) {
        MPI_Recv( &len, 1, MPI_UNSIGNED, MPI_ANY_SOURCE, MPI_ANY_TAG, comm, &status );
        src = status.MPI_SOURCE;
        
        if( len != 0 ) {
            if( len > expr_c_size ) {
                expr_c_size = len;
                if( NULL != expr_c ) free( expr_c );
                expr_c = (char*)malloc( expr_c_size ); // The \0 was added by the slave
            }

            /* Receive the result */
            MPI_Recv( expr_c, len, MPI_CHAR, src, TAG_EXPR, comm, &status );

            /* Put it in the result queue */
            results.push_back( std::string( expr_c ) );
        }
#ifdef SCATTERGATHER
        send_end( src, pzero );

#else
        /* Do not send the end signal yet */
#endif
        running--;
        
    }

    double t4 = getTime();

    /* Add whatever I have left */

    Tens = add_expressions_parall( results, symbols, pzero, comm );
    
    double t5 = getTime();
    std::cout << t2 - t1;
    std::cout << "\t " << t3 - t2;
    std::cout << "\t " << t4 - t3;
    std::cout << "\t " << t5 - t4 << std::endl;

#if DEBUG
    std::cout << "Received " << receivedresults << " results" << std::endl;

    std::cout << "Tpara=" << Tens << ";" << std::endl;
#endif
    
    if( NULL != expr_c) free( expr_c );
    return Tens;
}

void multiply_1level_slave_addslave4( tensor3D_t& T, unsigned int size, MPI_Comm comm = MPI_COMM_WORLD ) {
    gi::ex Tens;
    int  a2, a4;
    unsigned int len = 0;
    
    parameters_2_1_t params;
    MPI_Status status;
    char* expr_c;

    int rank;
    MPI_Comm_rank( comm, &rank );

    /* Ask for some work */
    
    MPI_Send( &len, 1, MPI_UNSIGNED, ROOT, TAG_PULL, comm );

    /* Compute the set of symbols */
    
    gi::lst symbols = all_symbols_3D( size );

    while( true ){
        /* Receive a set of parameters */

        MPI_Recv( &params, 1, DT_PARAMETERS_2_1, ROOT, MPI_ANY_TAG, comm, &status );
        
        if( status.MPI_TAG == TAG_WORK ){
            a4 = params.a4;
            a2 = params.a2;

            Tens = one_level1_product( &T, size, a4, a2 );

            send_result( Tens );

        } else {
            if( status.MPI_TAG == TAG_ADD ) {
                /* Receive a set of expressions to add */

                /* Number of expressions received */
                int nb = params.a4;
                a2 = params.a2;

                /* Length of each string */

                unsigned int* lengths = (unsigned int*) malloc( nb*sizeof( unsigned int ) );
                MPI_Recv( lengths, nb, MPI_INT, ROOT, TAG_ADD, comm, &status );
                std::vector<std::string> results_s;
                char* c_str;
                int i;
                int len;
                for( i = 0 ; i < nb ; i++ ) {
                    len = lengths[i] + 1 ;
                    c_str = (char*) malloc( len );
                    MPI_Recv( c_str, len - 1, MPI_CHAR, ROOT, TAG_ADD, comm, &status );
                    c_str[len - 1] = '\0';    // The master sends C++ strings, which do not contain the final '\0'
                    results_s.push_back( std::string( c_str ) );
                    free( c_str );
                }

                /* Delinearize all the expressions and add them */

                Tens = add_expressions( results_s, symbols );
                
                /* Send the result */

                send_result( Tens );

            } else {
                if( status.MPI_TAG == TAG_END ){
#ifdef SCATTERGATHER
                    std::vector<std::string> toto;
                    Tens = add_expressions_parall( toto, symbols, params, comm );
#endif
                    return;
                } else {
                    std::cerr << "Wrong tag received on slave " << status.MPI_TAG << std::endl;
                }
            }
        }
    }
}

/* Communication protocol:
   M -> W: always the same size, therefore unique communication
   W -> M: send an unsigned int (size of the expression), then the expression (table of chars)
*/
        
gi::ex multiply_1level_mw_addslave4( tensor3D_t& T, int size ) {  // simpler: same dimension everywhere
    int rank;
    gi::ex Tens = 0;
    MPI_Comm_rank( MPI_COMM_WORLD, &rank );

    /* Create a new datatype for the parameters */
    
    create_parameters_datatype_2_1();

    /* Here we go */

    if( 0 == rank ) {
        Tens = multiply_1level_master_addslave4( T, size );
    } else {
        multiply_1level_slave_addslave4( T, size );
    }

    /* Finalize */
    
    free_parameters_2_1_dt();
    return Tens;
}

